public class Child extends Parent{

    public Child() {
        super();
    }

    public Child(String name, int age) {
        super(name, age);
    }

    /*
    * Runtime Polymorphism
    */
    public void introduce(){
        System.out.println("I am a child!");
    }
}
