import java.util.ArrayList;

public class Phonebook extends Contact{
    ArrayList<String> contacts =  new ArrayList<>();

    public Phonebook(){
        super();
    }

    public Phonebook(String name, String contactNumber, String address, String contacts) {
        super(name, contactNumber, address);
        this.contacts.add(contacts);
    }

    public ArrayList<String> getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts.add(contacts);
    }

    public void browseContact(){
        super.browse();
    }
}
